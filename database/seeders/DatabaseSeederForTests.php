<?php

namespace Database\Seeders;

use App\Domain\Customers\Models\CustomerInfo;
use Illuminate\Database\Seeder;

class DatabaseSeederForTests extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        CustomerInfo::factory(10)->create();
    }
}
