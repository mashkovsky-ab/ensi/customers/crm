<?php

namespace Tests;

use Ensi\CustomerAuthClient\Api\UsersApi;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mockery\MockInterface;

class ComponentTestCase extends TestCase
{
    use DatabaseTransactions;

    protected function mockCustomerAuthUsersApi(): MockInterface|UsersApi
    {
        return $this->mock(UsersApi::class);
    }
}
