<?php

namespace App\Domain\Kafka\Messages\Listen\Tests;

use App\Domain\Kafka\Messages\Listen\Order\OrderEventMessage;
use Ensi\OmsClient\Dto\OrderStatusEnum;
use Ensi\TestFactories\Factory;
use RdKafka\Message;

class OrderEventMessageFactory extends Factory
{
    protected function definition(): array
    {
        $event = $this->faker->randomElement([
            OrderEventMessage::CREATE,
            OrderEventMessage::UPDATE,
            OrderEventMessage::DELETE,
        ]);

        return [
            'event' => $event,
            'attributes' => [
                'id' => $this->faker->randomNumber(),
                'status' => $this->faker->randomElement([
                    OrderStatusEnum::WAIT_PAY,
                    OrderStatusEnum::NEW,
                    OrderStatusEnum::APPROVED,
                    OrderStatusEnum::DONE,
                    OrderStatusEnum::CANCELED,
                ]),
                'customer_id' => $this->faker->randomNumber(),
            ],
            'dirty' => $event == OrderEventMessage::UPDATE ? $this->faker->randomElements([
                'status',
            ]) : null,
        ];
    }

    public function make(array $extra = [])
    {
        $message = new Message();
        $message->payload = json_encode($this->makeArray($extra));

        return $message;
    }

    protected function mergeDefinitionWithExtra(array $extra): array
    {
        $extraAttributes = $extra['attributes'] ?? [];
        unset($extra['attributes']);
        $array = parent::mergeDefinitionWithExtra($extra);

        $array['attributes'] = array_merge($array['attributes'], $extraAttributes);

        return $array;
    }
}
