<?php

namespace App\Domain\Kafka\Messages\Listen\Order;

use Illuminate\Support\Fluent;

/**
 * @property int $id
 * @property int $status
 * @property int $customer_id
 */
class OrderPayload extends Fluent
{
}
