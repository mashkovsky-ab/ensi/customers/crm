<?php

namespace App\Domain\Customers\Models;

use App\Domain\Customers\Models\Tests\Factories\PreferenceFactory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Класс-модель для сущности "Информация о предпочтениях"
 *
 * @property int $id
 * @property int $customer_id           - id пользователя
 * @property string $attribute_name     - Название аттрибута агрегации
 * @property string $attribute_value    - Значение аттрибута агрегации
 * @property float $product_count       - количество купленного товара
 * @property int $product_sum           - сумма затраченная на товары
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Preference extends Model
{
    /**
     * Заполняемые поля модели
     */
    const FILLABLE = ['customer_id', 'attribute_name', 'attribute_value', 'product_count', 'product_sum'];

    /**
     * @var array
     */
    protected $fillable = self::FILLABLE;

    /**
     * @var string
     */
    protected $table = 'preferences';

    public static function factory(): PreferenceFactory
    {
        return PreferenceFactory::new();
    }
}
