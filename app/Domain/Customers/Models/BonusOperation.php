<?php

namespace App\Domain\Customers\Models;

use App\Domain\Customers\Models\Tests\Factories\BonusOperationFactory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Класс-модель для сущности "История начислений/списаний"
 *
 * @property int $id
 * @property int $customer_id               - id пользователя
 * @property int|null $order_number         - Номер заказа
 * @property float $bonus_amount            - Количество начисленных/списанных бонусов
 * @property string|null $comment           - Комментарий
 * @property Carbon|null $activation_date   - Количество начисленных/списанных бонусов
 * @property Carbon|null $expiration_date   - Количество начисленных/списанных бонусов
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class BonusOperation extends Model
{
    /**
     * Заполняемые поля модели
     */
    const FILLABLE = ['customer_id', 'order_number', 'bonus_amount', 'comment', 'activation_date', 'expiration_date'];

    /**
     * @var array
     */
    protected $fillable = self::FILLABLE;

    /**
     * @var string
     */
    protected $table = 'bonus_operations';

    public static function factory(): BonusOperationFactory
    {
        return BonusOperationFactory::new();
    }

    /**
     * scope для отбора "Начисленных" баллов
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeEarning(Builder $query): Builder
    {
        return $query->where('bonus_amount', '>=', 0);
    }

    /**
     * scope для отбора "Списанных" баллов
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeSpending(Builder $query): Builder
    {
        return $query->where('bonus_amount', '<', 0);
    }
}
