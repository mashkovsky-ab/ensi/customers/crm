<?php

namespace App\Domain\Customers\Models;

use App\Domain\Customers\Models\Tests\Factories\FavoriteFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Класс-модель для сущности "Список избранного"
 *
 * @property int $id
 * @property int $customer_id
 * @property int $product_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property CustomerInfo $customer
 */
class Favorite extends Model
{
    protected $table = 'customer_favorites';

    const FILLABLE = ['customer_id', 'product_id'];

    /**
     * @var array
     */
    protected $fillable = self::FILLABLE;

    public static function factory(): FavoriteFactory
    {
        return FavoriteFactory::new();
    }

    public function customer(): BelongsTo
    {
        return $this->belongsTo(CustomerInfo::class);
    }
}
