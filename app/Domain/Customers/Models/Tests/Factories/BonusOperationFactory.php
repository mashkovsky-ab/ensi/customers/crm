<?php

namespace App\Domain\Customers\Models\Tests\Factories;

use App\Domain\Customers\Models\BonusOperation;
use Illuminate\Database\Eloquent\Factories\Factory;

class BonusOperationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BonusOperation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'customer_id' => $this->faker->unique()->randomNumber(),
            'order_number' => $this->faker->optional()->randomNumber(),
            'bonus_amount' => $this->faker->randomFloat(),
            'comment' => $this->faker->optional()->text(),
            'activation_date' => $this->faker->optional()->date(),
            'expiration_date' => $this->faker->optional()->date(),
        ];
    }
}
