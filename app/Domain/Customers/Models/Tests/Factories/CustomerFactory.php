<?php

namespace App\Domain\Customers\Models\Tests\Factories;

use App\Domain\Customers\Models\CustomerInfo;
use Illuminate\Database\Eloquent\Factories\Factory;

class CustomerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CustomerInfo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'customer_id' => $this->faker->unique()->randomNumber(),
            'kpi_sku_count' => $this->faker->optional()->randomNumber(),
            'kpi_sku_price_sum' => $this->faker->optional()->randomNumber(),
            'kpi_order_count' => $this->faker->optional()->randomNumber(),
            'kpi_shipment_count' => $this->faker->optional()->randomNumber(),
            'kpi_delivered_count' => $this->faker->optional()->randomNumber(),
            'kpi_delivered_sum' => $this->faker->optional()->randomNumber(),
            'kpi_refunded_count' => $this->faker->optional()->randomNumber(),
            'kpi_refunded_sum' => $this->faker->optional()->randomNumber(),
            'kpi_canceled_count' => $this->faker->optional()->randomNumber(),
            'kpi_canceled_sum' => $this->faker->optional()->randomNumber(),
        ];
    }
}
