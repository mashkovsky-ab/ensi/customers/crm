<?php

namespace App\Domain\Customers\Models\Tests\Factories;

use App\Domain\Customers\Models\ProductSubscribe;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductSubscribeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProductSubscribe::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'customer_id' => $this->faker->randomNumber(),
            'product_id' => $this->faker->randomNumber(),
        ];
    }
}
