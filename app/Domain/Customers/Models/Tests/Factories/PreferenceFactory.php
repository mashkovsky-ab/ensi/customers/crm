<?php

namespace App\Domain\Customers\Models\Tests\Factories;

use App\Domain\Customers\Models\Preference;
use Illuminate\Database\Eloquent\Factories\Factory;

class PreferenceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Preference::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'customer_id' => $this->faker->unique()->randomNumber(),
            'attribute_name' => $this->faker->text(),
            'attribute_value' => $this->faker->text(),
            'product_count' => $this->faker->randomNumber(),
            'product_sum' => $this->faker->randomNumber(),
        ];
    }
}
