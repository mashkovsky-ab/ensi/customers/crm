<?php

namespace App\Domain\Customers\Models;

use App\Domain\Customers\Models\Tests\Factories\ProductSubscribeFactory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Класс-модель для сущности "Подписка пользователя на товар"
 *
 * Class ProductSubscribe
 * @package App\Domain\Customers\Models
 *
 * @property int $id
 * @property int $customer_id               - ид покупателя
 * @property int $product_id                - ид покупателя
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class ProductSubscribe extends Model
{
    /**
     * Заполняемые поля модели
     */
    const FILLABLE = ['customer_id', 'product_id'];

    /**
     * @var array
     */
    protected $fillable = self::FILLABLE;

    /**
     * @var string
     */
    protected $table = 'product_subscribes';

    /**
     * @return ProductSubscribeFactory
     */
    public static function factory(): ProductSubscribeFactory
    {
        return ProductSubscribeFactory::new();
    }
}
