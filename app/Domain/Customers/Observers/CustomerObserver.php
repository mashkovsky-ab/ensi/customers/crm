<?php

namespace App\Domain\Customers\Observers;

use Ensi\CustomerAuthClient\Api\UsersApi;
use Ensi\CustomerAuthClient\ApiException;
use Ensi\CustomerAuthClient\Dto\PatchUserRequest;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class CustomerObserver
{
    public function __construct(protected EnsiFilesystemManager $filesystemManager, protected UsersApi $usersApi)
    {
    }

    /**
     * Handle the model "creating" event.
     * @param Customers $customer
     * @return void
     */
    public function creating(Customers $customer)
    {
        if (!$customer->status) {
            $customer->status = Customers::STATUS_CREATED;
        }
    }

    /**
     * Handle the model "updating" event.
     * @param Customers $customer
     * @return void
     *@todo разобраться почему Undefined class
     * @psalm-suppress all
     */
    public function updating(Customers $customer)
    {
        try {
            // Если статус изменился, то меняем активность пользователя в auth на правильную
            $statusChange = $customer->status != $customer->getOriginal('status');
            if ($statusChange) {
                $user = $this->usersApi->getUser($customer->user_id)->getData();
                $oldActive = $customer->getOriginal('status') == Customers::STATUS_BLOCK ? 0 : 1;
                $newActive = $customer->status == Customers::STATUS_BLOCK ? 0 : 1;
                if ($oldActive != $newActive) {
                    $patchUserRequest = new PatchUserRequest();
                    $patchUserRequest
                        ->setActive($customer->status == Customers::STATUS_BLOCK ? false : true);

                    if ($user->getActive() != $patchUserRequest->getActive()) {
                        $this->usersApi->patchUser($customer->user_id, $patchUserRequest);
                    }
                }
            }
        } catch (ApiException $e) {
            Log::warning("Customer auth request failed with errors: " . json_encode($e->getResponseErrors()));
        }
    }

    /**
     * Handle the model "deleting" event.
     * @param Customers $customer
     * @return void
     */
    public function deleting(Customers $customer)
    {
        try {
            $this->usersApi->deleteUser($customer->user_id);
        } catch (ApiException $e) {
            Log::warning("Delete user from customer auth request failed with errors: " . json_encode($e->getResponseErrors()));
        }

        foreach ($customer->addresses as $item) {
            $item->delete();
        }
    }

    /**
     * Handle the model "deleted" event.
     * @param Customers $customer
     * @return void
     */
    public function deleted(Customers $customer)
    {
        if ($customer->avatar) {
            Storage::disk($this->filesystemManager->publicDiskName())->delete($customer->avatar);
        }
    }
}
