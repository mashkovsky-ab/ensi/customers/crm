<?php

namespace App\Domain\Customers\Enums;

use Spatie\Enum\Laravel\Enum;

/**
 * @method static self CREATED()
 * @method static self NEW()
 * @method static self CONSIDERATION()
 * @method static self REJECTED()
 * @method static self ACTIVE()
 * @method static self PROBLEM()
 * @method static self BLOCK()
 * @method static self POTENTIAL_RP()
 * @method static self TEMPORALITY_SUSPENDED()
 */
class CustomerStatusEnum extends Enum
{
    protected static function values(): array
    {
        return [
            'CREATED' => 1,
            'NEW' => 2,
            'CONSIDERATION' => 3,
            'REJECTED' => 4,
            'ACTIVE' => 5,
            'PROBLEM' => 6,
            'BLOCK' => 7,
            'POTENTIAL_RP' => 8,
            'TEMPORALITY_SUSPENDED' => 9,
        ];
    }
}
