<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\Address;

class SetAddressAsDefaultAction
{
    public function execute(int $addressId): void
    {
        $address = Address::findOrFail($addressId);
        if ($address->default) {
            return;
        }

        Address::query()
            ->where('customer_id', $address->customer_id)
            ->where('default', true)
            ->update(['default' => false]);

        $address->default = true;
        $address->save();
    }
}
