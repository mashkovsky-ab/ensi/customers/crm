<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\Favorite;
use Illuminate\Support\Arr;

class CreateFavoriteAction
{
    public function execute(array $fields): Favorite
    {
        return Favorite::create(Arr::only($fields, Favorite::FILLABLE));
    }
}
