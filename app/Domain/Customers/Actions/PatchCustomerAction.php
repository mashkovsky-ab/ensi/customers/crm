<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\CustomerInfo;

class PatchCustomerAction
{
    public function execute(int $customerId, array $fields): CustomerInfo
    {
        $customer = CustomerInfo::findOrFail($customerId);
        $customer->update($fields);

        return $customer;
    }
}
