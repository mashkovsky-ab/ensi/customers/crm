<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\CustomerInfo;
use Illuminate\Support\Arr;

class PatchCustomersInfoAction
{
    public function execute(int $customerId, array $fields): CustomerInfo
    {
        $customer = CustomerInfo::findOrFail($customerId);
        $customer->update(Arr::only($fields, CustomerInfo::FILLABLE));

        return $customer;
    }
}
