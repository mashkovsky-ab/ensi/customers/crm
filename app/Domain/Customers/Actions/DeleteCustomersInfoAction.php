<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\CustomerInfo;

class DeleteCustomersInfoAction
{
    public function execute(int $customerInfoId): void
    {
        CustomerInfo::destroy($customerInfoId);
    }
}
