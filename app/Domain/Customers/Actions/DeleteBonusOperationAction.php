<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\BonusOperation;

class DeleteBonusOperationAction
{
    public function execute(int $bonusOperationId): void
    {
        BonusOperation::destroy($bonusOperationId);
    }
}
