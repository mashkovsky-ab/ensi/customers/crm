<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\BonusOperation;
use Illuminate\Support\Arr;

class ReplaceBonusOperationAction
{
    public function execute(int $bonusOperationId, array $fields): BonusOperation
    {
        $bonusOperation = BonusOperation::findOrFail($bonusOperationId);
        $bonusOperation->update(Arr::only($fields, BonusOperation::FILLABLE));

        return $bonusOperation;
    }
}
