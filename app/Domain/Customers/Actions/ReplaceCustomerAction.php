<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\CustomerInfo;

class ReplaceCustomerAction
{
    public function execute(int $customerId, array $fields): CustomerInfo
    {
        $customer = CustomerInfo::findOrFail($customerId);

        $newAttributes = $fields;
        foreach (CustomerInfo::FILLABLE as $attributeKey) {
            if (!array_key_exists($attributeKey, $newAttributes)) {
                $newAttributes[$attributeKey] = null;
            }
        }
        $customer->update($newAttributes);

        return $customer;
    }
}
