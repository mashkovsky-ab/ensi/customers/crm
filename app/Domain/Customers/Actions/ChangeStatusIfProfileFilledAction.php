<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\CustomerInfo;
use Ensi\CustomerAuthClient\Api\UsersApi;

class ChangeStatusIfProfileFilledAction
{
    /**
     * @var UsersApi
     */
    private $usersApi;

    public function __construct(UsersApi $usersApi)
    {
        $this->usersApi = $usersApi;
    }

    public function execute(int $customerId): CustomerInfo
    {
        $customer = CustomerInfo::findOrFail($customerId);

        if ($customer->status != CustomerInfo::STATUS_CREATED) {
            return $customer;
        }

        $user = $this->usersApi->getUser($customer->user_id)->getData();
        if (!$user->getPhone()) {
            return $customer;
        }

        $customer->status = CustomerInfo::STATUS_NEW;
        $customer->save();

        return $customer;
    }
}
