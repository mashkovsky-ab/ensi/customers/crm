<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\Favorite;

class ClearFavoritesAction
{
    public function execute(int $customerId): void
    {
        Favorite::query()
            ->where('customer_id', $customerId)
            ->delete();
    }
}
