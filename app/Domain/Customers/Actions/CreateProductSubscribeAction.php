<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\ProductSubscribe;
use Illuminate\Support\Arr;

class CreateProductSubscribeAction
{
    public function execute(array $fields): ProductSubscribe
    {
        return ProductSubscribe::create(Arr::only($fields, ProductSubscribe::FILLABLE));
    }
}
