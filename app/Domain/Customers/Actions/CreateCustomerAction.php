<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\CustomerInfo;
use Illuminate\Support\Arr;

class CreateCustomerAction
{
    public function execute(array $fields): CustomerInfo
    {
        return CustomerInfo::create(Arr::only($fields, CustomerInfo::FILLABLE));
    }
}
