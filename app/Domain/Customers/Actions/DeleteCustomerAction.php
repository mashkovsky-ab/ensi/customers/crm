<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\CustomerInfo;

class DeleteCustomerAction
{
    public function execute(int $customerId): void
    {
        CustomerInfo::destroy($customerId);
    }
}
