<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\Address;

class CreateAddressAction
{
    public function execute(array $fields): Address
    {
        $address = new Address();
        $address->customer_id = $fields['customer_id'];
        $address->default = $fields['default'] ?? false;
        $address->address = [
            'address_string' => $fields['address_string'],
            'post_index' => $fields['post_index'] ?? "",
            'country_code' => $fields['country_code'] ?? "",
            'region' => $fields['region'] ?? "",
            'region_guid' => $fields['region_guid'] ?? "",
            'area' => $fields['area'] ?? "",
            'area_guid' => $fields['area_guid'] ?? "",
            'city' => $fields['city'] ?? "",
            'city_guid' => $fields['city_guid'] ?? "",
            'street' => $fields['street'] ?? "",
            'house' => $fields['house'] ?? "",
            'block' => $fields['block'] ?? "",
            'porch' => $fields['porch'] ?? "",
            'intercom' => $fields['intercom'] ?? "",
            'floor' => $fields['floor'] ?? "",
            'flat' => $fields['flat'] ?? "",
            'comment' => $fields['comment'] ?? "",
            'geo_lat' => $fields['geo_lat'] ?? "",
            'geo_lon' => $fields['geo_lon'] ?? "",
        ];

        $address->save();

        return $address;
    }
}
