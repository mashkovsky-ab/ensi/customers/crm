<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\ProductSubscribe;

class DeleteProductFromProductSubscribesAction
{
    public function execute(int $customerId, int $productId): void
    {
        $favorite = ProductSubscribe::query()
            ->where('customer_id', $customerId)
            ->where('product_id', $productId)
            ->firstOrFail();

        $favorite->delete();
    }
}
