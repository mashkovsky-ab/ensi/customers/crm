<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\Favorite;

class DeleteProductFromFavoritesAction
{
    public function execute(int $customerId, int $productId): void
    {
        $favorite = Favorite::query()
            ->where('customer_id', $customerId)
            ->where('product_id', $productId)
            ->firstOrFail();

        $favorite->delete();
    }
}
