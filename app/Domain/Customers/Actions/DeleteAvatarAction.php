<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\CustomerInfo;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Support\Facades\Storage;
use RuntimeException;

class DeleteAvatarAction
{
    public function __construct(protected EnsiFilesystemManager $fileManager)
    {
    }

    public function execute(int $customerId): CustomerInfo
    {
        /** @var CustomerInfo $customer */
        $customer = CustomerInfo::findOrFail($customerId);

        if ($customer->avatar) {
            $disk = Storage::disk($this->fileManager->publicDiskName());
            if ($disk->exists($customer->avatar) && !$disk->delete($customer->avatar)) {
                throw new RuntimeException("Unable to delete file {$customer->avatar}");
            }

            $customer->avatar = null;
            $customer->save();
        }

        return $customer;
    }
}
