<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\YaCard;

class CreateYaCardAction
{
    public function execute(array $fields): YaCard
    {
        $card = new YaCard();
        $card->customer_id = $fields['customer_id'];
        $card->card_panmask = $fields['card_panmask'];
        $card->card_synonim = $fields['card_synonim'];
        $card->card_country_code = $fields['card_country_code'];
        $card->card_type = $fields['card_type'];
        $card->ya_account_number = $fields['ya_account_number'];
        $card->save();

        return $card;
    }
}
