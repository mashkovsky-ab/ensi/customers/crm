<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\BonusOperation;
use Illuminate\Support\Arr;

class CreateBonusOperationAction
{
    public function execute(array $fields): BonusOperation
    {
        return BonusOperation::create(Arr::only($fields, BonusOperation::FILLABLE));
    }
}
