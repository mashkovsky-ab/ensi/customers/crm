<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\Address;

class DeleteAddressAction
{
    public function execute(int $addressId): void
    {
        Address::destroy($addressId);
    }
}
