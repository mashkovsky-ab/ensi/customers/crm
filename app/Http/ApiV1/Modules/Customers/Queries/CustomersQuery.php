<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use App\Domain\Customers\Models\CustomerInfo;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class CustomersQuery extends QueryBuilder
{
    public function __construct()
    {
        $query = CustomerInfo::query();

        parent::__construct($query);

        $this->allowedIncludes(['addresses', 'favorites']);

        $this->allowedSorts(['id']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('user_id'),
            AllowedFilter::exact('status'),
        ]);

        $this->defaultSort('id');
    }
}
