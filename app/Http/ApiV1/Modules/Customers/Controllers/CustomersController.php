<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\ChangeStatusIfProfileFilledAction;
use App\Domain\Customers\Actions\CreateCustomerAction;
use App\Domain\Customers\Actions\DeleteCustomerAction;
use App\Domain\Customers\Actions\PatchCustomerAction;
use App\Domain\Customers\Actions\ReplaceCustomerAction;
use App\Http\ApiV1\Modules\Customers\Queries\CustomersQuery;
use App\Http\ApiV1\Modules\Customers\Requests\CreateOrReplaceCustomerRequest;
use App\Http\ApiV1\Modules\Customers\Requests\PatchCustomerRequest;
use App\Http\ApiV1\Modules\Customers\Resources\CustomersResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class CustomersController
{
    public function create(CreateOrReplaceCustomerRequest $request, CreateCustomerAction $action)
    {
        return new CustomersResource($action->execute($request->validated()));
    }

    public function replace(int $customerId, CreateOrReplaceCustomerRequest $request, ReplaceCustomerAction $action)
    {
        return new CustomersResource($action->execute($customerId, $request->validated()));
    }

    public function patch(int $customerId, PatchCustomerRequest $request, PatchCustomerAction $action)
    {
        return new CustomersResource($action->execute($customerId, $request->validated()));
    }

    public function delete(int $customerId, DeleteCustomerAction $action)
    {
        $action->execute($customerId);

        return new EmptyResource();
    }

    public function get(int $customerId, CustomersQuery $query)
    {
        return new CustomersResource($query->findOrFail($customerId));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, CustomersQuery $query)
    {
        return CustomersResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function changeStatusIfProfileFilled(int $customerId, ChangeStatusIfProfileFilledAction $action)
    {
        return new CustomersResource($action->execute($customerId));
    }
}
