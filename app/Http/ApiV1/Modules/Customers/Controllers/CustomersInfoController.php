<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\CreateCustomersInfoAction;
use App\Domain\Customers\Actions\DeleteCustomersInfoAction;
use App\Domain\Customers\Actions\PatchCustomersInfoAction;
use App\Http\ApiV1\Modules\Customers\Queries\CustomersInfoQuery;
use App\Http\ApiV1\Modules\Customers\Requests\CreateCustomersInfoRequest;
use App\Http\ApiV1\Modules\Customers\Requests\ReplaceCustomersInfoRequest;
use App\Http\ApiV1\Modules\Customers\Resources\CustomersInfoResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class CustomersInfoController
{
    public function create(CreateCustomersInfoRequest $request, CreateCustomersInfoAction $action)
    {
        return new CustomersInfoResource($action->execute($request->validated()));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, CustomersInfoQuery $query)
    {
        return CustomersInfoResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function get(int $customerId, CustomersInfoQuery $query)
    {
        return new CustomersInfoResource($query->findOrFail($customerId));
    }

    public function patch(int $customerId, ReplaceCustomersInfoRequest $request, PatchCustomersInfoAction $action)
    {
        return new CustomersInfoResource($action->execute($customerId, $request->validated()));
    }

    public function delete(int $customerId, DeleteCustomersInfoAction $action)
    {
        $action->execute($customerId);

        return new EmptyResource();
    }
}
