<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\CreateBonusOperationAction;
use App\Domain\Customers\Actions\DeleteBonusOperationAction;
use App\Domain\Customers\Actions\ReplaceBonusOperationAction;
use App\Http\ApiV1\Modules\Customers\Queries\BonusOperationsQuery;
use App\Http\ApiV1\Modules\Customers\Requests\CreateBonusOperationRequest;
use App\Http\ApiV1\Modules\Customers\Requests\ReplaceBonusOperationRequest;
use App\Http\ApiV1\Modules\Customers\Resources\BonusOperationsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class BonusOperationsController
{
    public function create(CreateBonusOperationRequest $request, CreateBonusOperationAction $action)
    {
        return new BonusOperationsResource($action->execute($request->validated()));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, BonusOperationsQuery $query)
    {
        return BonusOperationsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function get(int $id, BonusOperationsQuery $query)
    {
        return new BonusOperationsResource($query->findOrFail($id));
    }

    public function patch(int $id, ReplaceBonusOperationRequest $request, ReplaceBonusOperationAction $action)
    {
        return new BonusOperationsResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteBonusOperationAction $action)
    {
        $action->execute($id);

        return new EmptyResource();
    }
}
