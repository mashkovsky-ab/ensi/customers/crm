<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Http\ApiV1\Modules\Customers\Queries\PreferencesQuery;
use App\Http\ApiV1\Modules\Customers\Resources\PreferencesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;

class PreferencesController
{
    public function search(PageBuilderFactory $pageBuilderFactory, PreferencesQuery $query)
    {
        return PreferencesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
