<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\ClearFavoritesAction;
use App\Domain\Customers\Actions\CreateFavoriteAction;
use App\Domain\Customers\Actions\DeleteProductFromFavoritesAction;
use App\Http\ApiV1\Modules\Customers\Queries\FavoritesQuery;
use App\Http\ApiV1\Modules\Customers\Requests\ClearFavoritesRequest;
use App\Http\ApiV1\Modules\Customers\Requests\CreateFavoriteRequest;
use App\Http\ApiV1\Modules\Customers\Requests\DeleteProductFromFavoritesRequest;
use App\Http\ApiV1\Modules\Customers\Resources\FavoritesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class FavoritesController
{
    public function create(CreateFavoriteRequest $request, CreateFavoriteAction $action)
    {
        return new FavoritesResource($action->execute($request->validated()));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, FavoritesQuery $query)
    {
        return FavoritesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function deleteProduct(DeleteProductFromFavoritesRequest $request, DeleteProductFromFavoritesAction $action)
    {
        $action->execute($request->input('customer_id'), $request->input('product_id'));

        return new EmptyResource();
    }

    public function clear(ClearFavoritesRequest $request, ClearFavoritesAction $action)
    {
        $action->execute($request->input('customer_id'));

        return new EmptyResource();
    }
}
