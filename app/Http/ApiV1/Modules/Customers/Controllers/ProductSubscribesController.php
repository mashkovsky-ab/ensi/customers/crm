<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\ClearProductSubscribesAction;
use App\Domain\Customers\Actions\CreateProductSubscribeAction;
use App\Domain\Customers\Actions\DeleteProductFromProductSubscribesAction;
use App\Http\ApiV1\Modules\Customers\Queries\ProductSubscribesQuery;
use App\Http\ApiV1\Modules\Customers\Requests\ClearProductSubscribesRequest;
use App\Http\ApiV1\Modules\Customers\Requests\CreateProductSubscribeRequest;
use App\Http\ApiV1\Modules\Customers\Requests\DeleteProductFromProductSubscribesRequest;
use App\Http\ApiV1\Modules\Customers\Resources\ProductSubscribesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class ProductSubscribesController
{
    public function create(CreateProductSubscribeRequest $request, CreateProductSubscribeAction $action)
    {
        return new ProductSubscribesResource($action->execute($request->validated()));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, ProductSubscribesQuery $query)
    {
        return ProductSubscribesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function deleteProduct(DeleteProductFromProductSubscribesRequest $request, DeleteProductFromProductSubscribesAction $action)
    {
        $action->execute($request->input('customer_id'), $request->input('product_id'));

        return new EmptyResource();
    }

    public function clear(ClearProductSubscribesRequest $request, ClearProductSubscribesAction $action)
    {
        $action->execute($request->input('customer_id'));

        return new EmptyResource();
    }
}
