<?php

use App\Http\ApiV1\Modules\Customers\Controllers\BonusOperationsController;
use App\Http\ApiV1\Modules\Customers\Controllers\CustomersInfoController;
use App\Http\ApiV1\Modules\Customers\Controllers\FavoritesController;
use App\Http\ApiV1\Modules\Customers\Controllers\PreferencesController;
use App\Http\ApiV1\Modules\Customers\Controllers\ProductSubscribesController;
use Illuminate\Support\Facades\Route;

Route::post('customers-info', [CustomersInfoController::class, 'create']);
Route::post('customers-info:search', [CustomersInfoController::class, 'search']);
Route::get('customers-info/{id}', [CustomersInfoController::class, 'get']);
Route::patch('customers-info/{id}', [CustomersInfoController::class, 'patch']);
Route::delete('customers-info/{id}', [CustomersInfoController::class, 'delete']);

Route::post('favorites', [FavoritesController::class, 'create']);
Route::post('favorites:search', [FavoritesController::class, 'search']);
Route::post('favorites:delete-product', [FavoritesController::class, 'deleteProduct']);
Route::post('favorites:clear', [FavoritesController::class, 'clear']);

Route::post('product-subscribes', [ProductSubscribesController::class, 'create']);
Route::post('product-subscribes:search', [ProductSubscribesController::class, 'search']);
Route::post('product-subscribes:delete-product', [ProductSubscribesController::class, 'deleteProduct']);
Route::post('product-subscribes:clear', [ProductSubscribesController::class, 'clear']);

Route::post('preferences:search', [PreferencesController::class, 'search']);

Route::post('bonus-operations', [BonusOperationsController::class, 'create']);
Route::post('bonus-operations:search', [BonusOperationsController::class, 'search']);
Route::get('bonus-operations/{id}', [BonusOperationsController::class, 'get']);
Route::patch('bonus-operations/{id}', [BonusOperationsController::class, 'patch']);
Route::delete('bonus-operations/{id}', [BonusOperationsController::class, 'delete']);
