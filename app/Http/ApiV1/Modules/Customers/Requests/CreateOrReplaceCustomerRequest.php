<?php

namespace App\Http\ApiV1\Modules\Customers\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\CustomerGenderEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\CustomerStatusEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateOrReplaceCustomerRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'user_id' => ['required', 'integer', Rule::unique('customers')->ignore((int) $this->route('id'))],
            'status' => ['required', 'integer', Rule::in(CustomerStatusEnum::cases())],
            'birthday' => ['nullable', 'string'],
            'gender' => ['nullable', 'integer', Rule::in(CustomerGenderEnum::cases())],
            'comment_internal' => ['nullable', 'string'],
            'manager_id' => ['nullable', 'integer'],
            'avatar' => ['nullable', 'array'],
            'city' => ['nullable', 'string'],
            'comment_status' => ['nullable', 'string'],
            'referrer_id' => ['nullable', 'integer'],
            'referral_code' => ['nullable', 'string'],
            'referral_level_id' => ['nullable', 'integer'],
            'legal_info_company_name' => ['nullable', 'string'],
            'legal_info_company_address' => ['nullable', 'string'],
            'legal_info_inn' => ['nullable', 'string'],
            'legal_info_payment_account' => ['nullable', 'string'],
            'legal_info_bik' => ['nullable', 'string'],
            'legal_info_bank' => ['nullable', 'string'],
            'legal_info_bank_correspondent_account' => ['nullable', 'string'],
            'promo_page_name' => ['nullable', 'string'],
      ];
    }
}
