<?php

namespace App\Http\ApiV1\Modules\Customers\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class ReplaceBonusOperationRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'customer_id' => ['nullable', 'integer'],
            'order_number' => ['nullable', 'integer'],
            'bonus_amount' => ['nullable', 'numeric'],
            'comment' => ['nullable', 'string'],
            'activation_date' => ['nullable', 'date'],
            'expiration_date' => ['nullable', 'date'],
        ];
    }
}
