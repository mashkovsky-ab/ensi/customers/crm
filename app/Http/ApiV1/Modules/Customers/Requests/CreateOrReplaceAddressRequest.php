<?php

namespace App\Http\ApiV1\Modules\Customers\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateOrReplaceAddressRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'customer_id' => ['required', 'exists:customers,id'],
            'default' => ['boolean'],
            'address_string' => ['required', 'string'],
            'post_index' => ['nullable', 'string'],
            'country_code' => ['nullable', 'string'],
            'region' => ['nullable', 'string'],
            'region_guid' => ['nullable', 'string'],
            'area' => ['nullable', 'string'],
            'area_guid' => ['nullable', 'string'],
            'city' => ['nullable', 'string'],
            'city_guid' => ['required', 'string'],
            'street' => ['nullable', 'string'],
            'house' => ['nullable', 'string'],
            'block' => ['nullable', 'string'],
            'porch' => ['nullable', 'string'],
            'intercom' => ['nullable', 'string'],
            'floor' => ['nullable', 'string'],
            'flat' => ['nullable', 'string'],
            'comment' => ['nullable', 'string'],
            'geo_lat' => ['nullable', 'numeric'],
            'geo_lon' => ['nullable', 'numeric'],
        ];
    }
}
