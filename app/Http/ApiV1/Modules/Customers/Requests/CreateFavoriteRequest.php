<?php

namespace App\Http\ApiV1\Modules\Customers\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateFavoriteRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $customerId = (int)$this->input('customer_id');

        return [
            'customer_id' => ['required', 'integer'],
            'product_id' => [
                'required',
                'integer',
                Rule::unique('customer_favorites')->where('customer_id', "{$customerId}"),
            ],
        ];
    }
}
