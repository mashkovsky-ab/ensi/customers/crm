<?php

namespace App\Http\ApiV1\Modules\Customers\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateOrReplaceYaCardRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'customer_id' => ['required', 'integer'],
            'card_panmask' => ['required'. 'string'],
            'card_synonim' => ['required', 'string'],
            'card_country_code' =>  ['required'. 'string'],
            'card_type' =>  ['required'. 'string'],
            'ya_account_number' =>  ['required'. 'string'],
            'email' => Rule::unique('customer_ya_cards')->where(function ($query) {
                return $query->where('customer_id', '!=', (int) $this->input('customer_id'));
            }),
        ];
    }
}
