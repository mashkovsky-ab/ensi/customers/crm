<?php

namespace App\Http\ApiV1\Modules\Customers\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class UploadAvatarRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'file' => ['required', 'image', 'max:2048'],
        ];
    }
}
