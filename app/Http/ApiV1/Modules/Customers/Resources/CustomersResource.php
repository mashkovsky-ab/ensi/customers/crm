<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin \App\Domain\Customers\Models\CustomerInfo */
class CustomersResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'status' => $this->status,
            'birthday' => $this->birthday,
            'gender' => $this->gender,
            'comment_internal' => $this->comment_internal,
            'manager_id' => $this->manager_id,
            'city' => $this->city,
            'comment_status' => $this->comment_status,
            'referrer_id' => $this->referrer_id,
            'referral_code' => $this->referral_code,
            'referral_level_id' => $this->referral_level_id,
            'legal_info_company_name' => $this->legal_info_company_name,
            'legal_info_company_address' => $this->legal_info_company_address,
            'legal_info_inn' => $this->legal_info_inn,
            'legal_info_payment_account' => $this->legal_info_payment_account,
            'legal_info_bik' => $this->legal_info_bik,
            'legal_info_bank' => $this->legal_info_bank,
            'legal_info_bank_correspondent_account' => $this->legal_info_bank_correspondent_account,
            'promo_page_name' => $this->promo_page_name,
            'avatar' => $this->mapPublicFileToResponse($this->avatar),
            'addresses' => AddressesResource::collection($this->whenLoaded('addresses')),
            'favorites' => FavoritesResource::collection($this->whenLoaded('favorites')),
        ];
    }
}
