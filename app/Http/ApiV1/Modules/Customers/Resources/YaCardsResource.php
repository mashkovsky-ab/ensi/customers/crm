<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin \App\Domain\Customers\Models\YaCard */
class YaCardsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'card_panmask' => $this->card_panmask,
            'card_synonim' =>  $this->card_synonim,
            'card_country_code' =>  $this->card_country_code,
            'card_type' => $this->card_type,
            'ya_account_number' => $this->ya_account_number,
        ];
    }
}
