<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Domain\Customers\Models\ProductSubscribe;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/** @mixin ProductSubscribe */
class ProductSubscribesResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'product_id' => $this->product_id,
        ];
    }
}
