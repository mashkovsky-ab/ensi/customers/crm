<?php

use App\Domain\Customers\Enums\CustomerStatusEnum as DomainCustomerStatusEnum;
use App\Domain\Customers\Models\CustomerInfo;
use App\Domain\Customers\Models\Favorite;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\CustomerFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\CustomerStatusEnum as HttpCustomerStatusEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\CustomerAuthClient\Dto\EmptyDataResponse;
use Ensi\CustomerAuthClient\Dto\User;
use Ensi\CustomerAuthClient\Dto\UserResponse;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDeleted;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

/*uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/customers/customers success', function () {
    $customerData = CustomerFactory::new()->make();

    postJson('/api/v1/customers/customers', $customerData)
        ->assertStatus(201)
        ->assertJsonPath('data.user_id', $customerData['user_id']);

    assertDatabaseHas('customers', [
        'user_id' => $customerData['user_id'],
        'status' => $customerData['status'],
    ]);
});

test('PUT /api/v1/customers/customers/{id} success', function () {
    $customer = CustomerInfo::factory()->create(['manager_id' => 1, 'status' => DomainCustomerStatusEnum::NEW()]);
    $id = $customer->id;

    $managerId = 2;
    $customerData = CustomerFactory::new()->make([
        'manager_id' => $managerId,
        'status' => HttpCustomerStatusEnum::NEW,
    ]);

    putJson("/api/v1/customers/customers/$id", $customerData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $id)
        ->assertJsonPath('data.manager_id', $managerId)
        ->assertJsonPath('data.comment_internal', $customerData['comment_internal']);

    assertDatabaseHas('customers', [
        'id' => $id,
        'manager_id' => $managerId,
        'comment_internal' => $customerData['comment_internal'],
    ]);
});

test('PUT /api/v1/customers/customers/{id} 404', function () {
    putJson("/api/v1/customers/customers/11", CustomerFactory::new()->make())
        ->assertStatus(404)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test('PATCH /api/v1/customers/customers/{id} success', function () {
    $customer = CustomerInfo::factory()->create(['manager_id' => 1, 'comment_internal' => "Test"]);
    $id = $customer->id;

    $customerData = CustomerFactory::new()->only(['manager_id'])->make(['manager_id' => 2]);

    patchJson("/api/v1/customers/customers/$id", $customerData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $id)
        ->assertJsonPath('data.manager_id', $customerData['manager_id'])
        ->assertJsonPath('data.comment_internal', "Test");

    assertDatabaseHas('customers', [
        'id' => $id,
        'manager_id' => $customerData['manager_id'],
        'comment_internal' => "Test",
    ]);
});

test('PATCH /api/v1/customers/customers/{id} 404', function () {
    patchJson("/api/v1/customers/customers/11", CustomerFactory::new()->make())
        ->assertStatus(404)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test('DELETE /api/v1/customers/customers/{id} success', function () {
    $customer = CustomerInfo::factory()->create();
    $id = $customer->id;
    $this->mockCustomerAuthUsersApi()->allows([
        "deleteUser" => new EmptyDataResponse(),
    ]);

    deleteJson("/api/v1/customers/customers/$id")->assertStatus(200);

    assertDeleted($customer);
});

test('GET /api/v1/customers/customers/{id}?include=favorites success', function () {
    $customer = CustomerInfo::factory()->create();
    $favorites = Favorite::factory()->for($customer)->count(3)->create();
    $id = $customer->id;

    getJson("/api/v1/customers/customers/$id?include=favorites", )
        ->assertStatus(200)
        ->assertJsonPath('data.id', $id)
        ->assertJsonCount($favorites->count(), 'data.favorites');
});

test('GET /api/v1/customers/customers/{id} 404', function () {
    getJson("/api/v1/customers/customers/11")
        ->assertStatus(404)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test("POST /api/v1/customers/customers:search success", function () {
    $customers = CustomerInfo::factory()
        ->count(10)
        ->sequence(
            ['status' => DomainCustomerStatusEnum::CREATED()],
            ['status' => DomainCustomerStatusEnum::NEW()],
        )
        ->create();
    $lastId = $customers->last()->id;

    $requestBody = [
        "filter" => [
            "status" => HttpCustomerStatusEnum::NEW,
        ],
        "sort" => [
            "-id",
        ],
    ];

    postJson("/api/v1/customers/customers:search", $requestBody)
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $lastId)
        ->assertJsonPath('data.0.status', HttpCustomerStatusEnum::NEW);
});

test('POST /api/v1/customers/customers/{id}:change-status-if-profile-filled does NOTHING if customer status is not CREATED', function (string $statusLabel) {
    $status = DomainCustomerStatusEnum::from($statusLabel);
    $customer = CustomerInfo::factory()->create(['status' => $status]);
    $id = $customer->id;

    $this->mockCustomerAuthUsersApi()->shouldNotReceive('getUser');

    postJson("/api/v1/customers/customers/$id:change-status-if-profile-filled")
        ->assertStatus(200);

    assertDatabaseHas('customers', [
        'id' => $id,
        'status' => $status,
    ]);
})->with(
    collect(DomainCustomerStatusEnum::toLabels())
        ->reject(DomainCustomerStatusEnum::CREATED()->label)
        ->toArray()
);

test('POST /api/v1/customers/customers/{id}:change-status-if-profile-filled changes status to NEW if phone is filled', function () {
    $customer = CustomerInfo::factory()->create(['status' => DomainCustomerStatusEnum::CREATED()]);
    $id = $customer->id;

    $this->mockCustomerAuthUsersApi()->allows([
        "getUser" => new UserResponse([
            'data' => new User(['id' => $id, 'phone' => '+78181215152']),
        ]),
    ]);

    postJson("/api/v1/customers/customers/$id:change-status-if-profile-filled")
        ->assertStatus(200);

    assertDatabaseHas('customers', [
        'id' => $id,
        'status' => DomainCustomerStatusEnum::NEW(),
    ]);
});*/
