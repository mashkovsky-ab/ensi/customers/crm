<?php

use App\Domain\Customers\Models\CustomerInfo;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

use function Pest\Laravel\post;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

//test('POST /api/v1/customers/customers/{id}:upload-avatar success', function (string $extension) {
//    $diskName = resolve(EnsiFilesystemManager::class)->publicDiskName();
//    Storage::fake($diskName);
//
//    $customer = CustomerInfo::factory()->create();
//    $id = $customer->id;
//
//    $requestBody = ['file' => UploadedFile::fake()->create("avatar." . $extension, kilobytes: 100)];
//
//    $response = post("/api/v1/customers/customers/$id:upload-avatar", $requestBody, ['Content-Type' => "multipart/form-data"]);
//
//    $response->assertStatus(200);
//    $responseAvatar = $response->decodeResponseJson()['data']['avatar'];
//
//    /** @var \Illuminate\Filesystem\FilesystemAdapter */
//    $disk = Storage::disk($diskName);
//    $disk->assertExists($responseAvatar['path']);
//    expect(CustomerInfo::query()->where('id', $id)->whereNotNull('avatar')->exists())->toBeTrue();
//})->with(
//    ['jpg', 'jpeg', 'png', 'bmp', 'gif', 'svg', 'webp']
//);
//
//test('POST /api/v1/customers/customers/{id}:upload-avatar fail if wrong extension', function (string $extension) {
//    $customer = CustomerInfo::factory()->create();
//    $id = $customer->id;
//
//    $requestBody = ['file' => UploadedFile::fake()->create("avatar." . $extension, kilobytes: 100)];
//
//    post("/api/v1/customers/customers/$id:upload-avatar", $requestBody, ['Content-Type' => "multipart/form-data"])
//        ->assertStatus(400)
//        ->assertJsonPath('errors.0.code', "ValidationError");
//
//    expect(CustomerInfo::query()->where('id', $id)->whereNotNull('avatar')->exists())->toBeFalse();
//})->with(['doc', 'pdf']);
//
//test('POST /api/v1/customers/customers/{id}:upload-avatar fail if size exceeds 2mb', function () {
//    $customer = CustomerInfo::factory()->create();
//    $id = $customer->id;
//
//    $requestBody = ['file' => UploadedFile::fake()->create("avatar.jpg", kilobytes: 3000)];
//
//    post("/api/v1/customers/customers/$id:upload-avatar", $requestBody, ['Content-Type' => "multipart/form-data"])
//        ->assertStatus(400)
//        ->assertJsonPath('errors.0.code', "ValidationError");
//
//    expect(CustomerInfo::query()->where('id', $id)->whereNotNull('avatar')->exists())->toBeFalse();
//});
//
//test('POST /api/v1/customers/customers/{id}:upload-avatar 404 if customer not found', function () {
//    $requestBody = ['file' => UploadedFile::fake()->create("avatar.jpg", kilobytes: 100)];
//
//    post("/api/v1/customers/customers/11:upload-avatar", $requestBody, ['Content-Type' => "multipart/form-data"])
//        ->assertStatus(404)
//        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
//});
//
//test('POST /api/v1/customers/customers/{id}:delete-avatar success', function (bool $dbExists, bool $fileExists) {
//    $avatarPath = "avatars/avatar.jpg";
//    $diskName = resolve(EnsiFilesystemManager::class)->publicDiskName();
//    Storage::fake($diskName);
//    if ($fileExists) {
//        Storage::disk($diskName)->put($avatarPath, 'some content');
//    }
//
//    $customerFactory = CustomerInfo::factory();
//    if ($dbExists) {
//        $customerFactory = $customerFactory->withAvatar($avatarPath);
//    }
//    $customer = $customerFactory->create();
//    $id = $customer->id;
//
//    $response = postJson("/api/v1/customers/customers/$id:delete-avatar", []);
//
//    $response
//        ->assertStatus(200)
//        ->assertJsonPath('data.id', $id)
//        ->assertJsonPath('data.avatar', null);
//
//    /** @var \Illuminate\Filesystem\FilesystemAdapter */
//    $disk = Storage::disk($diskName);
//    $disk->assertMissing($avatarPath);
//    expect(CustomerInfo::query()->where('id', $id)->whereNotNull('avatar')->exists())->toBeFalse();
//})->with([
//    ['dbExists' => true, 'fileExists' => true],
//    ['dbExists' => true, 'fileExists' => false],
//    ['dbExists' => false, 'fileExists' => false],
//]);
