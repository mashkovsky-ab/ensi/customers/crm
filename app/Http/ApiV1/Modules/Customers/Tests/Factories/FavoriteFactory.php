<?php

namespace App\Http\ApiV1\Modules\Customers\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class FavoriteFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->optionalId(),
            'customer_id' => $this->faker->randomNumber(),
            'address_string' => $this->faker->address(),
            'city_guid' => $this->faker->uuid(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
