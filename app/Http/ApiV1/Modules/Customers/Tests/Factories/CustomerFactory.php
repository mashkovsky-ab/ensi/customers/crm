<?php

namespace App\Http\ApiV1\Modules\Customers\Tests\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\CustomerGenderEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\CustomerStatusEnum;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use App\Http\ApiV1\Support\Tests\Factories\FileFactory;
use Ensi\TestFactories\FactoryMissingValue;

class CustomerFactory extends BaseApiFactory
{
    public ?FileFactory $avatarFactory = null;
    public ?array $addressFactories = null;
    public ?array $favoriteFactories = null;

    protected function definition(): array
    {
        return [
            'id' => $this->optionalId(),
            'user_id' => $this->faker->unique()->randomNumber(),
            'status' => $this->faker->randomElement(CustomerStatusEnum::cases()),
            'birthday' => $this->faker->optional()->date(),
            'gender' => $this->faker->randomElement(CustomerGenderEnum::cases()),
            'comment_internal' => $this->faker->optional()->text(),
            'manager_id' => $this->faker->optional()->randomNumber(),
            'avatar' => $this->avatarFactory?->make(),
            'city' => $this->faker->optional()->city(),
            'comment_status' => $this->faker->optional()->text(),
            'legal_info_company_name' => $this->faker->optional()->company(),
            'legal_info_company_address' => $this->faker->optional()->address(),
            'legal_info_inn' => $this->faker->optional()->numerify('#############'),
            'legal_info_payment_account' => $this->faker->optional()->numerify('#############'),
            'legal_info_bik' => $this->faker->optional()->numerify('#############'),
            'legal_info_bank' => $this->faker->optional()->company(),
            'legal_info_bank_correspondent_account' => $this->faker->optional()->numerify('#############'),
            'addresses' => $this->executeNested($this->addressFactories, new FactoryMissingValue()),
            'favorites' => $this->executeNested($this->favoriteFactories, new FactoryMissingValue()),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function withAvatar(FileFactory $factory = null): self
    {
        return $this->immutableSet('avatarFactory', $factory ?? FileFactory::new());
    }

    public function includesAddresses(?array $factories = null): self
    {
        return $this->immutableSet('addressFactories', $factories ?? [CustomerAddressFactory::new()]);
    }

    public function includesFavorites(?array $factories = null): self
    {
        return $this->immutableSet('favoriteFactories', $factories ?? [FavoriteFactory::new()]);
    }
}
