<?php

namespace App\Http\ApiV1\Modules\Customers\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class CustomerAddressFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->optionalId(),
            'customer_id' => $this->faker->randomNumber(),
            'product_id' => $this->faker->randomNumber(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
