<?php

namespace App\Console\Commands\Preferences;

use App\Domain\Customers\Actions\SyncPreferenceAction;
use Illuminate\Console\Command;
use Throwable;

class SyncPreferences extends Command
{
    /** @inerhitDoc */
    protected $signature = 'preferences:sync';
    /** @inerhitDoc */
    protected $description = 'Command to synchronise preferences according to configuration';

    /**
     * Execute the console command.
     */
    public function handle(SyncPreferenceAction $action): void
    {
        try {
            $action->execute();
        } catch (Throwable $e) {
            logger()->error($e->getMessage(), $e->getTrace());
            $this->output->error($e->getMessage());
        }
    }
}
