<?php

namespace App\Console\Commands\Kafka;

use Ensi\CustomerAuthClient\Dto\User;
use Ensi\LaravelPhpRdKafkaProducer\HighLevelProducer;
use Illuminate\Console\Command;
use Throwable;

class SendExampleKafkaMessage extends Command
{
    /** @inerhitDoc */
    protected $signature = 'kafka:send-example-message';
    /** @inerhitDoc  */
    protected $description = 'Send a message with default data to'
    . ' <contour>.communication.test.example.1 kafka topic'
    . ' (hint: check your config cache if processor for topic exists and not found)';

    /**
     * Execute the console command.
     */
    public function handle(): int
    {
        $subscribe = new User([
            'full_name' => 'full_name',
            'short_name' => 'short_name',
            'active' => true,
            'login' => 'login',
            'last_name' => 'lastName',
            'first_name' => 'firstName',
            'middle_name' => 'middleName',
            'email' => 'email',
            'phone' => 'phone',
            'timezone' => 'timezone',
        ]);

        try {
            (new HighLevelProducer(topic('crm.test.example.1')))->sendOne($subscribe->__toString());
        } catch (Throwable $e) {
            $this->error('An error occurred while sending a message to kafka');
            $this->line($e->getMessage());

            return 1;
        }

        $this->line('The message was sent successfully');

        return 0;
    }
}
